# GP-EOT

Made by Nicola Castellani (**VR361872**), for the _Master Game Dev Graphics Programming course_, University of Verona.

## `Introduction`

The goal is to create a clone of the classic **Arcanoid** game, using DirectX libraries for the rendering part (_like sprite and text rendering_).

---

### **_The Final Solution_**

The project uses [DirectXTK12](https://github.com/Microsoft/DirectXTK12/wiki/Getting-Started) from Microsoft official repositories, so the _DirectX_ lifecycle is handled by the toolkit.

The toolkit gives an _interface between DirectX and the programmer_, abstracting from DirectX initialization and management.

This solution is the best, in my opinion, thanks to these **_pros_**:

1. DirectX library **_ease of use_**
2. **_Well written documentation_**
3. **_Concrete examples_** on GitHub
4. **_From DirectX11 to DirectX12_** migration helpers
5. Set of **_tutorials/tools_** explained well
6. Both **_Microsoft desktop_** and **_UWP app_**
7. **_Huge set of templates_** for Visual Studio

### **_Other attempts_**

I've tried several solutions before choosing DirectXTK:

1. Pure DirectX11 development
2. Pure DirectX12 development
3. Official and Unofficial tutorials

All of these results in a _non-optimized, hard to read code_. The creation of **devices, effects, batches and shaders** is pretty hard and could result in a big mess (due to missing or hard to understand documentation).

---

## `Project Structure`

- **/Assets**  
  Contains graphic assets (Fonts, Sprites, etc..)
- **/Common**  
  Shared classes, contains DirectX12 lib
- **/Core**  
  Core classes and utils for the engine
- **/Custom**  
  Custom implementation of core classes
- **/Scenes**  
  Scenes in game

---

## `Getting Started`

Just open the project in Visual Studio, and run either in Debug or Release mode.

Or run **_Arcanoid.exe_** inside _/Debug_ or _/Release_ folders inside the project root, after building the project.

---

## `The Engine`

### **Core objects**

`Main.cpp` and `Game.h`

My implementations results on a **_Unity like_** custom engine.

Everything starts from a **_Game instance_**, created inside **Main.cpp** (the core cpp file of the project), after window process creation.

The Game instance handles:

- **Singleton initialization** (ScreenManager, InputManager, SceneManager, etc..)
- **App lifecycle** (Initialization, Update, Rendering, Window resizing, etc..)
- DirectX **Device Resources** management
- **Device and Window dependent** resources management

### **_Scene_**

`Scene.h`

The scene is one of the core elements of the engine, handles **GameObjects** creation and lifecycle.

### **_GameObject_**

`GameObject.h`

A **_GameObject_** is an object composed by a **_Transform_** and a collection of **_Components_**.

#### Transform

`Transform.h`

Handles position, rotation and scale of a GameObject.

#### Component

`Component.h`

A component is a plug-in object ready to be reused in different GameObjects.

### **_Rendering_**

The 2 core components for rendering are:

- `SpriteRenderer.h` : uses DirectX _sprite batch_ to render textures on screen.
- `TextRenderer.h` : uses DirectX _sprite batch_ to render fonts on screen.
- `DebugBoxRenderer.h` : Draws a debug box for collision testing and origin of a GameObject. Enable **_DEBUG_** in `pch.h` to see the result.

### **_Physic_**

The physic is **_simulated_**, the ball speed is calculated according to collided object speed and collision point.

The collisions are handled using [**_Bounding Boxes_**](https://developer.mozilla.org/en-US/docs/Games/Techniques/2D_collision_detection) (check out `Utils.h`).

### **_Singletons_**

#### Debugger

`Debugger.h`  
Draws text to screen using **DX::TextConsole**, useful to print logs.

#### InputManager

`InputManager.h`  
Handles keyboard input.

#### ScreenManager

`ScreenManager.h`  
Keeps screen information, like dimensions.

### Engine

`Engine.h`  
Keeps game object reference, contains a utility for game object queries.

---

## `Future Improvements`

1. Improve **_Scene management_**, currently is pretty awful and the unload of the scene is not safe (a thread sleep helps the process but is a temporary fix).

2. Improve **_physics_**, currently is just a simulation and needs a lot of improvements.

3. Adds **_responsive management_** to the engine.
4. Adds **_mouse management_**
5. Improve **_pointer management_**, **_caching_** and other performance boost improvements.
6. Adds **_.dds_** support (currently working, but the _.dds_ files are 400% heavier than the _.png_ counterpart, sometimes).

---

## `References`

- [DirectXTK12 Wiki](https://github.com/Microsoft/DirectXTK12/wiki/Getting-Started)
- [2D Collision Detection](https://developer.mozilla.org/en-US/docs/Games/Techniques/2D_collision_detection)

---

## `Tools`

- [DirectXTex](https://github.com/microsoft/DirectXTex/releases)
- [MakeSpriteFont](https://github.com/microsoft/DirectXTK/wiki/MakeSpriteFont)
- [Gravit Designer Pro](https://www.designer.io/it/)
