#include "pch.h"
#include "Level03.h"

void Level03::Load(const DeviceResources deviceResources)
{
	SetId(L"Level 0-3");
	BaseLevel::Load(deviceResources);
}
void Level03::CreateBricks()
{
	for (int j = 0; j < 8; j++) {
		for (int i = -4; i <= 4; ++i) {
			if (j > 0 && j < 7 && i != -4 && i != 4) continue;
			BaseLevel::CreateBrick(Vector2(i * COL_SIZE, -300 + j * ROW_SIZE));
		}
	}
}