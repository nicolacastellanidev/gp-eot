#include "pch.h"
#include "Background.h"
#include "SpriteRenderer.h"

Background::Background()
{
	m_id = L"Background";
}

void Background::Awake()
{
	// 	AddComponent(std::make_shared<SpriteRenderer>(SpriteRenderer({ L"./Assets/Sprites/background.dds" })));
	AddComponent(std::make_shared<SpriteRenderer>(SpriteRenderer({ L"./Assets/Sprites/background.png" })));
	GameObject::Awake();
}
