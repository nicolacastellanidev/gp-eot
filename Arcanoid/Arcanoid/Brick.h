#pragma once
#include "GameObject.h"

#ifndef RED_BRICK
#define RED_BRICK L"./Assets/Sprites/brick_red.png"
#endif

#ifndef YELLOW_BRICK
#define YELLOW_BRICK L"./Assets/Sprites/brick_yellow.png"
#endif

enum class BRICK_TYPE {
	RED,
	YELLOW
};

class Brick :
	public GameObject
{

	typedef DirectX::SimpleMath::Vector2 Vector2;

public:
	Brick();
	void SetType(BRICK_TYPE type);
	void Awake() override;

private:
	BRICK_TYPE m_type = BRICK_TYPE::RED;
	const wchar_t* GetSprite();
};

