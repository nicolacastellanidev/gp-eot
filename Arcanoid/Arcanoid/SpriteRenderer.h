#pragma once
#include "Component.h"

class SpriteRenderer: public Component
{
	typedef DirectX::SimpleMath::Vector2 Vector2;
	typedef std::unique_ptr<DirectX::DescriptorHeap> DescriptorHeapRef;
	typedef std::shared_ptr<DX::DeviceResources> DeviceResourcesRef;
	typedef Microsoft::WRL::ComPtr<ID3D12Resource> TextureRef;
	typedef std::unique_ptr<DirectX::SpriteBatch> SpriteBatchRef;

	enum Descriptors
	{
		Sprite,
		Count
	};
	/**
	 * Data for sprite
	 * @author nicola.castellani
	 */
	struct SpriteProperties {
		const wchar_t* texture;
		Vector2 origin = { 0, 0 };
	};

public:
	SpriteRenderer(const SpriteProperties& properties);

	Vector2 GetSize() const;
	void SetSprite(const wchar_t* sprite);
protected:
	// lifecycle
	void LoadResources(const DeviceResourcesRef deviceResources) override;
	void LoadTexture(bool firstTime = true);
	void Reset() override;
	virtual void Render(const DeviceResourcesRef deviceResources) override;

private:

	SpriteProperties m_properties;
	Vector2 m_size;

	DescriptorHeapRef m_resourceDescriptors;
	DeviceResourcesRef m_deviceResources;
	TextureRef m_texture;
	SpriteBatchRef m_spriteBatch;

};