#include "pch.h"
#include "Level01.h"

void Level01::Load(const DeviceResources deviceResources)
{
	SetId(L"Level 0-1");
	BaseLevel::Load(deviceResources);
}
void Level01::CreateBricks()
{
	for (int j = 0; j < 3; j++) {
		for (int i = -3; i <= 3; ++i) {
			BaseLevel::CreateBrick(Vector2(i * COL_SIZE, -200 + j * ROW_SIZE));
		}
	}
}