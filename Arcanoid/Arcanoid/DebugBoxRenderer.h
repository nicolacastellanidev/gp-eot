#pragma once
#include "Component.h"
#include <memory.h>

class DebugBoxRenderer :
    public Component
{

	typedef std::unique_ptr<DirectX::DescriptorHeap> DescriptorHeapRef;
	typedef std::unique_ptr<DirectX::BasicEffect> BasicEffectRef;
	typedef std::unique_ptr<DirectX::PrimitiveBatch<DirectX::VertexPositionColor>> BatchRef;

	enum Descriptors
	{
		DebugBox,
		Count
	};

protected:
	void LoadResources(const std::shared_ptr<DX::DeviceResources> deviceResources) override;
	void Reset() override;
	virtual void Render(const std::shared_ptr<DX::DeviceResources> deviceResources) override;
private:
	BatchRef m_debugBatch;
	DescriptorHeapRef m_resourceDescriptors;
	BasicEffectRef m_effect;
};

