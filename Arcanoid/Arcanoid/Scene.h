#pragma once
#include "DeviceResources.h"
#include "GameObject.h"
#include <memory>

/**
* A Scene is a collection of GameObjects, handles creation and lifecycle
*/
class Scene
{
	typedef std::shared_ptr<DX::DeviceResources> DeviceResources;
	typedef std::shared_ptr<GameObject> GameObjectPtr;
	typedef std::vector<GameObjectPtr> GameObjects;

public:
	/**
	* Calls LoadResources and Awake for each child game object 
	* @author nicola.castellani
	*/
	virtual void Load(const DeviceResources deviceResources);
	/**
	* Dispose game objects
	* @author nicola.castellani
	*/
	virtual void Unload();
	/**
	* Getter for bUnloaded
	* @author nicola.castellani
	*/
	virtual bool IsUnloaded();

	// gameobjects lifecycle, called from SceneManager
	virtual void Reset();
	virtual void Start();
	virtual void Update();
	virtual void FixedUpdate();
	void Render(const DeviceResources deviceResources);

protected:

	virtual void SetId(wchar_t* id);

	GameObjects m_gameObjects;
	wchar_t* m_id = L"Scene";
	bool bUnloaded = false;
	bool bStarted = false;
};

