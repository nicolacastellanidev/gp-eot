#include "pch.h"
#include "SceneManager.h"
#include "DeviceResources.h"
#include "Game.h"
#include "Engine.h"
#include "Level01.h"
#include "Level02.h"
#include "Level03.h"
#include <Windows.h>
SceneManager* SceneManager::instance = 0;
SceneManager::SceneManager()
{
	m_scenes.push_back(std::make_shared<Level01>(Level01()));
	m_scenes.push_back(std::make_shared<Level02>(Level02()));
	m_scenes.push_back(std::make_shared<Level03>(Level03()));
	m_sceneIndex = 0;
}

SceneManager* SceneManager::GetInstance()
{
	if (instance == 0) {
		instance = new SceneManager();
	}
	assert(instance != nullptr);
	return instance;
}

void SceneManager::Init(DeviceResourcesRef deviceResources, Game* game)
{
	GetInstance()->m_game = game;
	instance->m_scenes[instance->m_sceneIndex]->Load(deviceResources);
	instance->m_deviceResources = deviceResources;
}

void SceneManager::Render(DeviceResourcesRef deviceResources)
{
	if (GetInstance()->m_loadingScene || instance->m_scenes[instance->m_sceneIndex]->IsUnloaded())
		return;
	instance->m_scenes[instance->m_sceneIndex]->Render(deviceResources);
}

void SceneManager::Start()
{
	if (GetInstance()->m_loadingScene || instance->m_scenes[instance->m_sceneIndex]->IsUnloaded())
		return;
	instance->m_scenes[instance->m_sceneIndex]->Start();
}

void SceneManager::Update()
{
	if (GetInstance()->m_loadingScene || instance->m_scenes[instance->m_sceneIndex]->IsUnloaded())
		return;
	instance->m_scenes[instance->m_sceneIndex]->Update();
}

void SceneManager::FixedUpdate()
{
	if (GetInstance()->m_loadingScene || instance->m_scenes[instance->m_sceneIndex]->IsUnloaded())
		return;
	instance->m_scenes[instance->m_sceneIndex]->FixedUpdate();
}

void SceneManager::Reset()
{
	if (GetInstance()->m_loadingScene || instance->m_scenes[instance->m_sceneIndex]->IsUnloaded())
		return;
	instance->m_scenes[instance->m_sceneIndex]->Reset();
}

void SceneManager::LoadNextScene()
{
	if (GetInstance()->m_loadingScene)
		return;

	instance->m_loadingScene = true;
	instance->m_game->PauseTick();

	instance->m_scenes[instance->m_sceneIndex]->Unload();
	instance->m_sceneIndex++;
	instance->m_scenes[instance->m_sceneIndex]->Load(instance->m_deviceResources);
	Start();
}

void SceneManager::ReloadCurrentScene()
{
	instance->m_scenes[instance->m_sceneIndex]->Unload();
	instance->m_scenes[instance->m_sceneIndex]->Load(instance->m_deviceResources);
	Start();
}

void SceneManager::SceneLoaded()
{
	instance->m_game->PauseTick(false);
	GetInstance()->m_loadingScene = false;
}

bool SceneManager::IsLoading()
{
	return GetInstance()->m_loadingScene;
}
