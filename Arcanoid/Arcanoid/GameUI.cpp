#include "pch.h"
#include "GameUI.h"
#include "UIText.h"
#include "HeartIcon.h"
#include "Engine.h"
#include "Player.h"

#ifndef TOP_Y
#define TOP_Y -375.0f
#endif
/*
empty --- title --- hearths

*/
GameUI::GameUI(const wchar_t* levelName)
	:m_player(nullptr)
{
	m_id = L"GameUI";
	// title
	auto title = std::make_shared<UIText>(UIText(levelName));
	title->SetPosition({ 0, TOP_Y });
	m_childrens.push_back(title);

	
	// hearths
	for (int i = 0; i < 3; ++i) {
		auto heartIcon = std::make_shared<HeartIcon>(HeartIcon());
		heartIcon->SetPosition({ 360.0f + i * 50.0f, TOP_Y });
		m_heartIcons.push_back(heartIcon);
		m_childrens.push_back(heartIcon);
	}
}

void GameUI::Start()
{
	m_player = reinterpret_cast<Player*>(Engine::GetGameObjectWithTag(TAG::PLAYER));
	m_player->OnHit(std::bind(&GameUI::AfterPlayerHit, this));
}

void GameUI::AfterPlayerHit()
{
	int lives = m_player->GetLives();
	for (int i = 0; i < m_heartIcons.size(); ++i) {
		m_heartIcons[i]->SetSprite(i <= lives - 1 ? DEFAULT_SPRITE : EMPTY_SPRITE);
	}
}
