#include "pch.h"
#include "BallController.h"
#include "SpriteRenderer.h"
#include "GameObject.h"
#include "ScreenManager.h"
#include "Debugger.h"
#include "Engine.h"
#include "Player.h"
#include <cassert>

BallController::BallController(std::shared_ptr<SpriteRenderer> sr)
{
	m_sr = sr;
}

void BallController::OnCollisionWithPlayer(const Vector2& playerPosition, float playerXSpeed)
{
	m_direction.y *= -1;
	auto m_pos = m_gameObject->GetPosition();
	m_direction.x = playerPosition.x < m_pos.x ? 1.0f : -1.0f;
	m_speed.x = BASE_SPEED + abs(m_pos.x - playerPosition.x) / abs(20.0f - playerXSpeed);
	m_speed.y = -BASE_SPEED + abs(m_pos.x - playerPosition.x) / 20.0f;
}

void BallController::OnCollisionWithBrick(const Vector2& brickPosition)
{
	auto m_pos = m_gameObject->GetPosition();

	m_direction.x = brickPosition.x <= m_pos.x ? 1.0f : -1.0f;
	m_direction.y = brickPosition.y <= m_pos.y ? -1.0f : 1.0f;

	m_speed.x = BASE_SPEED + abs(m_pos.x - brickPosition.x) / 20.0f;
	m_speed.y = -BASE_SPEED + abs(m_pos.x - brickPosition.x) / 20.0f;
}

void BallController::SetHorizontalDirection(int next)
{
	m_direction.x = next ? next / next : 0.0f; // prevent value different from 1, -1 and 0
}

float BallController::Radius()
{
	return m_radius;
}

const BallController::Vector2& BallController::GetSize()
{
	return m_size;
}

const BallController::Vector2& BallController::GetDirection()
{
	return m_direction;
}

void BallController::Start()
{
	m_size = m_sr->GetSize();
	m_direction = { 0, -1 };
	m_radius = m_size.x;
	m_player = reinterpret_cast<Player*>(Engine::GetGameObjectWithTag(TAG::PLAYER));
	assert(m_player != nullptr);
}

void BallController::Update()
{
	auto position = m_gameObject->GetPosition();
	float screenWidth = ScreenManager::GetWidth();
	float screenHeight = ScreenManager::GetHeight();

	position.x += m_direction.x * m_speed.x;
	position.y += m_direction.y * m_speed.y;

	if (position.x < 0 + m_size.x / 2 || position.x > screenWidth - m_size.x / 2) {
		m_direction.x *= -1;
	}

	if (position.y < 0 + m_size.y / 2) {
		m_direction.y *= -1;
	}

	if (position.y > screenHeight - m_size.y / 2) {
		m_direction.y *= -1;
		m_player->Hit();
	}

	m_gameObject->SetPosition(position);
}
