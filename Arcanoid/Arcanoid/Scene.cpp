#include "pch.h"
#include "Scene.h"
#include "GameObject.h"
#include "Debugger.h"
#include "Engine.h"
#include "SceneManager.h"
#include <Windows.h>

void Scene::Load(const DeviceResources deviceResources)
{
	for (GameObjectPtr go : m_gameObjects) {
		go->Awake();
		go->LoadResources(deviceResources);
	}
	bUnloaded = false;
}

void Scene::Unload()
{
	bUnloaded = true;
	for (GameObjectPtr go : m_gameObjects) {
		go->DestroyImmediate();
		go.reset();
	}
	// wait a little bit for resource deallocation @todo how to handle this without a timer?
	Sleep(250);
	Engine::ResetGameObjects();
	m_gameObjects.clear();
}

void Scene::Reset()
{
	if (bUnloaded) return;
	for (int i = 0; i < m_gameObjects.size(); ++i) {
		m_gameObjects[i]->Reset();
		m_gameObjects[i].reset();
	}
}

void Scene::Start()
{
	if (bUnloaded) return;
	for (GameObjectPtr go : m_gameObjects) {
		if (!go->bDestroyed) go->Start();
	}
	bStarted = true;
}

void Scene::Update()
{
	if (bUnloaded || !bStarted) return;
	for (GameObjectPtr go : m_gameObjects) {
		if (!go->bDestroyed) go->Update();
	}
}

void Scene::FixedUpdate()
{
	if (bUnloaded) return;
	for (GameObjectPtr go : m_gameObjects) {
		if(!go->bDestroyed) go->FixedUpdate();
	}
}

bool Scene::IsUnloaded()
{
	return bUnloaded;
}

void Scene::Render(const DeviceResources deviceResources)
{
	if (bUnloaded) return;
	for (GameObjectPtr go : m_gameObjects) {
		go->Render(deviceResources);
	}
}

void Scene::SetId(wchar_t* id)
{
	m_id = id;
}
