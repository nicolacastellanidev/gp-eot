#include "pch.h"
#include "Engine.h"
#include "GameObject.h"

Engine* Engine::instance = 0;
Engine* Engine::GetInstance()
{
	if (instance == 0) {
		instance = new Engine();
	}
	return instance;
}

void Engine::AddGameObject(GameObject* go)
{
	GetInstance()->m_gameObjects.push_back(go);
}

GameObject* Engine::GetGameObjectWithTag(TAG tag)
{
	for each (GameObject* go in instance->m_gameObjects)
	{
		if (go->CompareTag(tag)) {
			return go;
		}
	}

	return nullptr;
}

void Engine::ResetGameObjects()
{
	instance->m_gameObjects.clear();
}
