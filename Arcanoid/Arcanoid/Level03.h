#pragma once
#include "BaseLevel.h"

class Level03 :
	public BaseLevel
{
private:
	void Load(const DeviceResources deviceResources) override;
	void CreateBricks() override;
};