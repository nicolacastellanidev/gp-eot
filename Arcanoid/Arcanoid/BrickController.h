#pragma once
#include "Component.h"

class SpriteRenderer;
class Ball;

class BrickController
	: public Component
{
	typedef DirectX::SimpleMath::Vector2 Vector2;

public:
	BrickController(std::shared_ptr<SpriteRenderer> sr);
protected:
	void Start() override;
	void FixedUpdate() override;
	void CheckBallPosition();
	std::shared_ptr<SpriteRenderer> m_sr;

private:
	Vector2 m_size;
	Ball* m_ball;
};

