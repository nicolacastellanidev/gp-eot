#include "pch.h"
#include "InputManager.h"
#include "Debugger.h"

InputManager* InputManager::instance = 0;

typedef DirectX::Keyboard Keyboard;

InputManager* InputManager::GetInstance()
{
	if (instance == 0) {
		instance = new InputManager();
	}
	return instance;
}

void InputManager::Setup(HWND window)
{
	// force instance to be defined
	GetInstance()->m_window = std::make_unique<HWND>(window);
	instance->m_keyboard = std::make_unique<Keyboard>();
}

DirectX::Keyboard::State InputManager::GetKeyboardState()
{
	return instance->m_keyboard->GetState();
}

int InputManager::GetAxisRaw(AXIS toCheck)
{
	auto kb = GetKeyboardState();
	switch (toCheck)
	{
	case AXIS::HORIZONTAL:
		return kb.A || kb.Left ? -1 : (kb.D || kb.Right ? 1 : 0);
	default:
		wchar_t debug_out[8];
		swprintf_s(debug_out, L"%d", toCheck);
		Debugger::Print(L"InputManager", L"Cannot check axis:", debug_out, LOG_SEVERITY::WARN);
		return 0;
	}
}
