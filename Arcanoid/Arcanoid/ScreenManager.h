#pragma once
class ScreenManager
{
	static ScreenManager* instance;
	struct Vector2 {
		float X, Y;
		Vector2 operator/(float amount) {
			return { X / amount, Y / amount };
		}
	};
public:
	ScreenManager();
	static ScreenManager* GetInstance();
	static float GetWidth();
	static float GetHeight();
	static float GetHalfWidth();
	static float GetHalfHeight();

private:
	Vector2 m_screen_size{ 1000,800 };
	// cache half of the width to prevent a lot of divisions on static values
	Vector2 m_screen_size_half;
};

