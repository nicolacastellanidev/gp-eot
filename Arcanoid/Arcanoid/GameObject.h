#pragma once
#include "Component.h"
#include "DeviceResources.h"
#include "Transform.h"
#include <memory>
#include <vector>

// FW
enum class TAG;

class GameObject
{
	friend class Scene;

	typedef DirectX::SimpleMath::Vector2 Vector2;

	typedef std::function<void(void)> DeathCallback;
	typedef std::vector<DeathCallback> DeathCallbacks;

	typedef std::shared_ptr<Transform> TransformRef;

	typedef std::shared_ptr<Component> ComponentRef;
	typedef std::vector<ComponentRef> Components;

	typedef std::shared_ptr<GameObject> GameObjectRef;
	typedef std::vector<GameObjectRef> GameObjects;

	typedef std::shared_ptr<DX::DeviceResources> DeviceResourcesRef;

public:
	GameObject();
	GameObject(const Vector2& position);
	GameObject(const Vector2& position, float rotation);
	GameObject(const Vector2& position, float rotation, const Vector2& scale);

	~GameObject();
	GameObject(const GameObject& rhs) = default;
	GameObject& operator=(const GameObject& rhs) = default;

	// components
	Components GetComponents() const;

	// transform related
	TransformRef GetTransform() const;
	// position
	const Vector2& GetPosition() const;
	void SetPosition(const Vector2& position) const;
	// rotation
	float GetRotation() const;
	void SetRotation(float rotation) const;
	// scale
	const Vector2& GetScale() const;
	void SetScale(const Vector2& scale) const;

	// size (handled by components, default is 1)
	const Vector2& GetSize() const;
	void SetSize(const Vector2& size);

	// tag
	void SetTag(TAG tag);
	bool CompareTag(TAG tag);

	// lifecycle
	void LoadResources(const DeviceResourcesRef deviceResources);
	/**
	* Reset components and pointers
	* @author nicola.castellani
	*/
	void Reset();
	/**
	* Called after scene creation
	* @author nicola.castellani
	*/
	virtual void Awake();
	/**
	* Called after game starts
	* @author nicola.castellani
	*/
	virtual void Start();
	/**
	* Called after pause triggered
	* @author nicola.castellani
	*/
	void Stop();
	/**
	* Called each game loop tick, if not paused
	* @author nicola.castellani
	*/
	void Update();
	/**
	* Called each window loop tick, if not paused
	* @author nicola.castellani
	*/
	void FixedUpdate();
	/**
	* Called after Update
	* @author nicola.castellani
	*/
	void Render(const DeviceResourcesRef deviceResources);
	/**
	* Destroy the game object
	* @author nicola.castellani
	*/
	void Destroy();
	/**
	* Like Destroy, but skips callback calls
	* @author nicola.castellani
	*/
	void DestroyImmediate();
	/**
	* Sets a callback to call after go destroy
	* @author nicola.castellani
	*/
	void OnDestroy(DeathCallback cb);
	/**
	* Attach and set a new component
	* @author nicola.castellani
	*/
	void AddComponent(ComponentRef component);

protected:
	GameObjects m_childrens;
	const wchar_t* m_id = L"GameObject";
	TAG m_tag;

private:
	Vector2 m_size{ 1,1 };
	DeathCallbacks m_onDeathCb;
	TransformRef m_transform;
	Components m_components;

	bool bDestroyed = false;
	bool bStopped = false;
};