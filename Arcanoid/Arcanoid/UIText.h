#pragma once
#include "GameObject.h"
class UIText :
    public GameObject
{

public:
    UIText(const wchar_t* text);
    void Awake() override;

private:
    const wchar_t* m_text;
};

