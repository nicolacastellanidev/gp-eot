﻿#include "pch.h"
#include "Debugger.h"
#include "Transform.h"

using namespace DirectX;
Debugger* Debugger::instance = 0;

const wchar_t* ToString(const LOG_SEVERITY Log)
{
	switch (Log)
	{
	default:
	case LOG_SEVERITY::LOG:
		return L"Info";
	case LOG_SEVERITY::WARN:
		return L"Warning";
	case LOG_SEVERITY::ERR:
		return L"Error";
	}
}

Debugger* Debugger::GetInstance()
{
	if (instance == 0) {
		instance = new Debugger();
	}
	return instance;
}

void Debugger::Init(const DeviceResourcesRef deviceResources)
{
	GetInstance();
	auto device = deviceResources->GetD3DDevice();
	ResourceUploadBatch resourceUpload(device);

	instance->m_resourceDescriptors = std::make_unique<DescriptorHeap>(device,
		Descriptors::Count);

	resourceUpload.Begin();

	RenderTargetState rtState(deviceResources->GetBackBufferFormat(),
		deviceResources->GetDepthBufferFormat());

	instance->console = std::make_unique<DX::TextConsole>();
	instance->console->RestoreDevice(device, resourceUpload, rtState,
		L"./Assets/Fonts/courier-new-regular.spritefont",
		instance->m_resourceDescriptors->GetCpuHandle(Descriptors::Font),
		instance->m_resourceDescriptors->GetGpuHandle(Descriptors::Font));

	auto uploadResourcesFinished = resourceUpload.End(
		deviceResources->GetCommandQueue());

	uploadResourcesFinished.wait();
}

void Debugger::Render(const DeviceResourcesRef deviceResources)
{
	auto commandList = deviceResources->GetCommandList();
	auto heap = instance->m_resourceDescriptors->Heap();
	commandList->SetDescriptorHeaps(1, &heap);
	instance->console->Render(commandList);
}

void Debugger::SetConsoleViewport(const DeviceResourcesRef deviceResources)
{
	auto viewport = deviceResources->GetScreenViewport();
	instance->console->SetViewport(viewport);
	RECT size = deviceResources->GetOutputSize();
	instance->console->SetWindow(SimpleMath::Viewport::ComputeTitleSafeArea(size.right, size.bottom));
}

void Debugger::Reset()
{
	instance->console->ReleaseDevice();
}

void Debugger::Print(const wchar_t* Context, const wchar_t* Message, const LOG_SEVERITY Log) 
{
	if (instance->m_rows >= CONSOLE_ROW_LIMIT) {
		instance->m_rows = 0;
		instance->console->Clear();
	}
	instance->m_rows++;
	instance->console->SetForegroundColor(GetConsoleAttributes(Log));
	instance->console->Format(L"[%s] [%s] >>> %s\n", ToString(Log), Context, Message);
}

void Debugger::Print(const wchar_t* Context, const wchar_t* Message, const wchar_t* Description, const LOG_SEVERITY Log)
{
	wchar_t debug_out[256];
	swprintf_s(debug_out, L"%s, %s", Message, Description);
	Print(Context, debug_out, Log);
}

void Debugger::Print(const wchar_t* Context, const wchar_t* Message, const Vector2& Vector, const LOG_SEVERITY Log)
{
	wchar_t debug_out[256];
	swprintf_s(debug_out, L"{%f, %f}", Vector.x, Vector.y);
	Print(Context, Message, debug_out, Log);
}

void Debugger::Print(const wchar_t* Context, const wchar_t* Message, float value, const LOG_SEVERITY Log)
{
	wchar_t debug_out[256];
	swprintf_s(debug_out, L"%f", value);
	Print(Context, Message, debug_out, Log);
}

void Debugger::Clear()
{
	instance->console->Clear();
}

DirectX::XMVECTORF32 Debugger::GetConsoleAttributes(const LOG_SEVERITY Log)
{
	switch (Log)
	{
	default:
	case LOG_SEVERITY::LOG:
		return Colors::White;
	case LOG_SEVERITY::WARN:
		return Colors::Orange;
	case LOG_SEVERITY::ERR:
		return Colors::Red;
	}
}
