#include "pch.h"
#include "Component.h"
#include "DeviceResources.h"
#include "GameObject.h"

void Component::Destroy()
{
	bDeleted = true;
}

const GameObject& Component::GetGameObject()
{
	return *m_gameObject;
}

void Component::Set(GameObject& gameObject)
{
	m_gameObject = &gameObject;
}
