#pragma once
#include "GameObject.h"

class SpriteRenderer;
class BallController;

class Ball :
	public GameObject
{
	typedef DirectX::SimpleMath::Vector2 Vector2;
public:
	Ball();
	void Awake() override;
	const Vector2& GetDirection();
	const Vector2& GetSize();
	void OnCollisionWithPlayer(const Vector2& playerPosition, float playerXSpeed);
	void OnCollisionWithBrick(const Vector2& brickPosition);

private:
	std::shared_ptr<SpriteRenderer> m_sr;
	std::shared_ptr<BallController> m_bc;
};

