#pragma once
#include "Component.h"

#ifndef DEFAULT_TXT_SHADOW
#define DEFAULT_TXT_SHADOW {{ {0, 2}, DirectX::Colors::Black }}
#endif

#ifndef DEFAULT_TXT_OUTLINE
#define DEFAULT_TXT_OUTLINE {{ {1, 1}, DirectX::Colors::Black }}
#endif

class TextRenderer :
    public Component
{
	typedef DirectX::SimpleMath::Vector2 Vector2;
	typedef std::shared_ptr<DX::DeviceResources> DeviceResourcesRef;
	typedef std::unique_ptr<DirectX::DescriptorHeap> DescriptorHeapRef;
	typedef std::unique_ptr<DirectX::SpriteFont> SpriteFontRef;
	typedef std::unique_ptr<DirectX::SpriteBatch> SpriteBatchRef;

	enum Descriptors
	{
		Font,
		Count
	};

	struct Effect {
		Vector2 delta;
		DirectX::XMVECTORF32 color;
	};
	typedef std::vector<Effect> Effects;

	struct TextProperties {
		Effects dropShadows;
		Effects outlines;
		const wchar_t* font;
		const wchar_t* text;
	};

public:
	TextRenderer(const TextProperties& properties);

protected:
	// lifecycle
	void LoadResources(const DeviceResourcesRef deviceResources) override;
	void Reset() override;
	virtual void Render(const DeviceResourcesRef deviceResources) override;

	virtual void DrawShadows();
	virtual void DrawOutlines();

private:
	TextProperties m_properties;
	DescriptorHeapRef m_resourceDescriptors;
	SpriteFontRef m_font;
	SpriteBatchRef m_spriteBatch;
	Vector2 m_origin{ 0, 0 };
};

