#include "pch.h"
#include "Level02.h"

void Level02::Load(const DeviceResources deviceResources)
{
	SetId(L"Level 0-2");
	BaseLevel::Load(deviceResources);
}
void Level02::CreateBricks()
{
	for (int j = 0; j < 7; ++j) {
		for (int i = -4; i <= 4; ++i) {
			if (j >= 1 && j <= 4 && (i == 0 || i == -4 || i == 4) ) continue;
			BaseLevel::CreateBrick(Vector2(i * COL_SIZE, -300 + j * ROW_SIZE));
		}
	}
}