#include "pch.h"
#include "ScreenManager.h"

ScreenManager* ScreenManager::instance = 0;

ScreenManager::ScreenManager()
{
	m_screen_size_half = m_screen_size / 2;
}

ScreenManager* ScreenManager::GetInstance()
{
	if (instance == 0) {
		instance = new ScreenManager();
	}
	return instance;
}

float ScreenManager::GetWidth()
{
	return GetInstance()->m_screen_size.X;
}

float ScreenManager::GetHeight()
{
	return GetInstance()->m_screen_size.Y;
}

float ScreenManager::GetHalfWidth()
{
	return GetInstance()->m_screen_size_half.X;
}

float ScreenManager::GetHalfHeight()
{
	return GetInstance()->m_screen_size_half.Y;
}
