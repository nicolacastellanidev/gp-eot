#pragma once
#include <memory>

enum class AXIS {
	HORIZONTAL,
	VERTICAL
};

class InputManager
{
	static InputManager* instance;

	typedef std::unique_ptr<DirectX::Keyboard> KeyboardRef;
	typedef std::unique_ptr<HWND> WindowRef;

public:
	static InputManager* GetInstance();
	/**
	* Load keyboard and mouse, set m_window instance
	* @author nicola.castellani
	*/
	static void Setup(HWND window);
	/**
	* Return current keyboard state
	* @author nicola.castellani
	*/
	static DirectX::Keyboard::State GetKeyboardState();
	/**
	* Returns 1 if AXIS is pressed (positive), -1 if AXIS is pressed (negative), 0 else
	* @author nicola.castellani
	*/
	static int GetAxisRaw(AXIS toCheck);

private:
	KeyboardRef m_keyboard;
	WindowRef m_window;
};

