#pragma once
#include "BaseLevel.h"

class Level01 :
    public BaseLevel
{
private:
	void Load(const DeviceResources deviceResources) override;
	void CreateBricks() override;
};