#include "pch.h"
#include "Player.h"
#include "SpriteRenderer.h"
#include "PlayerController.h"
#include "Engine.h"
#include "DebugBoxRenderer.h"

Player::Player()
{
	m_id = L"Player";
}

void Player::Awake()
{
	// auto sr = std::make_shared<SpriteRenderer>(SpriteRenderer({ L"./Assets/Sprites/player.dds" }));
	auto sr = std::make_shared<SpriteRenderer>(SpriteRenderer({ L"./Assets/Sprites/player.png" }));

	SetTag(TAG::PLAYER);

	AddComponent(sr);
	AddComponent(std::make_shared<PlayerController>(PlayerController(sr)));

#ifdef DEBUG
	AddComponent(std::make_shared<DebugBoxRenderer>(DebugBoxRenderer()));
#endif // DEBUG


	Engine::AddGameObject(this); // register
}

void Player::Hit()
{
	m_lives--;

	for each (auto cb in m_onHitCb)
	{
		cb();
	}

	if (m_lives <= 0) {
		Destroy();
	}
}

void Player::OnHit(std::function<void(void)> cb)
{
	m_onHitCb.push_back(cb);
}

int Player::GetLives() const
{
	return m_lives;
}
