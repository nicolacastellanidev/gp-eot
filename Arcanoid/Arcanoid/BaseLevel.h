#pragma once
#include "Scene.h"

#ifndef COL_SIZE
#define COL_SIZE 70.0f
#endif

#ifndef ROW_SIZE
#define ROW_SIZE 45.0f
#endif

class BallController;

class BaseLevel :
    public Scene
{

protected:
	typedef DirectX::SimpleMath::Vector2 Vector2;
	typedef std::shared_ptr<DX::DeviceResources> DeviceResources;

public:
	void Load(const DeviceResources deviceResources) override;
	void Unload() override;
protected:

	virtual void LoadScene();
	virtual void CreateBricks();
	virtual void CreateBrick(const Vector2& position);
	virtual void AfterBrickDestroy();
	virtual void AfterPlayerDestroy();

	DeviceResources m_deviceResources;

	int brickCount = 0;
	int playerHealth = 3;
};

