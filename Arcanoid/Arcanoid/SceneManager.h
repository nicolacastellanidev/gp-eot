#pragma once
#include <memory.h>

namespace DX {
	class DeviceResources;
}

class Scene;
class Game;
class SceneManager
{
	static SceneManager* instance;

	typedef std::shared_ptr<DX::DeviceResources> DeviceResourcesRef;
	typedef std::vector<std::shared_ptr<Scene>> Scenes;
public:
	SceneManager();
	static SceneManager* GetInstance();
	/**
	* Store game instance and load first level
	* @author nicola.castellani
	*/
	static void Init(DeviceResourcesRef deviceResources, Game* game);
	static void Render(DeviceResourcesRef deviceResources);
	static void Start();
	static void Update();
	static void FixedUpdate();
	static void Reset();

	static void LoadNextScene();
	static void ReloadCurrentScene();

	static void SceneLoaded();
	static bool IsLoading();
private:
	Scenes m_scenes;
	DeviceResourcesRef m_deviceResources;
	Game* m_game;
	int m_sceneIndex = 0;
	bool m_loadingScene = false;
};

