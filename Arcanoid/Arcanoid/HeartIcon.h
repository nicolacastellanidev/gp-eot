#pragma once
#include "GameObject.h"
class SpriteRenderer;

class HeartIcon
	: public GameObject
{

#ifndef DEFAULT_SPRITE
#define DEFAULT_SPRITE L"./Assets/Sprites/hearts.png"
#endif

#ifndef EMPTY_SPRITE
#define EMPTY_SPRITE L"./Assets/Sprites/hearts-empty.png"
#endif

public:
	HeartIcon();
	void Awake() override;
	void SetSprite(const wchar_t* sprite);

private:
	std::shared_ptr<SpriteRenderer> m_sr;
};

