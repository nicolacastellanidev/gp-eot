#include "pch.h"
#include "UIText.h"
#include "TextRenderer.h"

UIText::UIText(const wchar_t* text)
{
	m_id = L"Text";
	m_text = text;
}

void UIText::Awake()
{
	AddComponent(std::make_shared<TextRenderer>(
		TextRenderer({ DEFAULT_TXT_SHADOW, DEFAULT_TXT_OUTLINE, L"./Assets/Fonts/courier-new-regular.spritefont", m_text })
	));
}
