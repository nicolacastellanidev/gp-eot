#pragma once
#include "GameObject.h"
class Player :
    public GameObject
{
	typedef DirectX::SimpleMath::Vector2 Vector2;
public:
	Player();
	void Awake() override;
	void Hit();
	void OnHit(std::function<void(void)> cb);
	int GetLives() const;
private:
	std::vector<std::function<void(void)>> m_onHitCb;
	int m_lives = 3;
};

