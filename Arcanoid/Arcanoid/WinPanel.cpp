#include "pch.h"
#include "WinPanel.h"
#include "SpriteRenderer.h"
#include "WinPanelController.h"

WinPanel::WinPanel()
{
	m_id = L"WinPanel";
}

void WinPanel::Awake()
{
	// AddComponent(std::make_shared<SpriteRenderer>(SpriteRenderer({ L"./Assets/Sprites/winpanel.dds" })));
	AddComponent(std::make_shared<SpriteRenderer>(SpriteRenderer({ L"./Assets/Sprites/winpanel.png" })));
	AddComponent(std::make_shared<WinPanelController>(WinPanelController()));

	GameObject::Awake();
}
