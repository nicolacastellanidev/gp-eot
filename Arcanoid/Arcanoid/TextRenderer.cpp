#include "pch.h"
#include "TextRenderer.h"
#include "GameObject.h"
#include "Debugger.h"
#include "Game.h"
#include <memory>

using namespace DirectX;

TextRenderer::TextRenderer(const TextProperties& properties)
{
	m_properties = properties;
}

void TextRenderer::LoadResources(const DeviceResourcesRef deviceResources)
{
	auto device = deviceResources->GetD3DDevice();

	m_resourceDescriptors = std::make_unique<DescriptorHeap>(device,
		Descriptors::Count);

	ResourceUploadBatch resourceUpload(device);

	resourceUpload.Begin();

	try {
		m_font = std::make_unique<SpriteFont>(device, resourceUpload,
			m_properties.font,
			m_resourceDescriptors->GetCpuHandle(Descriptors::Font),
			m_resourceDescriptors->GetGpuHandle(Descriptors::Font));
	}
	catch (...) {
		Debugger::Print(L"TextRenderer", L"Cannot find font", m_properties.font, LOG_SEVERITY::ERR);
		auto uploadResourcesFinished = resourceUpload.End(
			deviceResources->GetCommandQueue());
		uploadResourcesFinished.wait();
		bDeleted = true;
		return;
	}


	RenderTargetState rtState(deviceResources->GetBackBufferFormat(),
		deviceResources->GetDepthBufferFormat());

	SpriteBatchPipelineStateDescription pd(rtState);
	m_spriteBatch = std::make_unique<SpriteBatch>(device, resourceUpload, pd);

	auto viewport = deviceResources->GetScreenViewport();
	m_spriteBatch->SetViewport(viewport);

	auto size = deviceResources->GetOutputSize();
	Vector2 nextPosition = m_gameObject->GetPosition();

	nextPosition.x += float(size.right) / 2.f;
	nextPosition.y += float(size.bottom) / 2.f;

	m_gameObject->SetPosition(nextPosition);

	auto uploadResourcesFinished = resourceUpload.End(
		deviceResources->GetCommandQueue());

	uploadResourcesFinished.wait();
}

void TextRenderer::Reset()
{
	m_font.reset();
	m_resourceDescriptors.reset();
	m_spriteBatch.reset();
}

void TextRenderer::Render(const DeviceResourcesRef deviceResources)
{
	ID3D12DescriptorHeap* heaps[] = { m_resourceDescriptors->Heap() };

	auto commandList = deviceResources->GetCommandList();
	commandList->SetDescriptorHeaps(static_cast<UINT>(std::size(heaps)), heaps);

	m_spriteBatch->Begin(commandList);
	// update origin dinamically, as the text might change
	m_origin = m_font->MeasureString(m_properties.text) / 2.f;

	if (m_properties.outlines.size() > 0) {
		DrawOutlines();
	}

	if (m_properties.dropShadows.size() > 0) {
		DrawShadows();
	}

	m_font->DrawString(
		m_spriteBatch.get(),
		m_properties.text,
		m_gameObject->GetPosition(),
		Colors::White, 
		m_gameObject->GetRotation(),
		m_origin,
		m_gameObject->GetScale()
	);

	m_spriteBatch->End();
}

void TextRenderer::DrawShadows()
{
	for (int i = m_properties.dropShadows.size() - 1; i >= 0; --i) {

		m_font->DrawString(
			m_spriteBatch.get(),
			m_properties.text,
			m_gameObject->GetPosition() + Vector2(m_properties.dropShadows[i].delta),
			m_properties.dropShadows[i].color,
			m_gameObject->GetRotation(),
			m_origin,
			m_gameObject->GetScale()
		);
	}
}

void TextRenderer::DrawOutlines()
{
	auto m_spriteBatchGet = m_spriteBatch.get();
	Vector2 position = m_gameObject->GetPosition();
	float rotation = m_gameObject->GetRotation();
	Vector2 scale = m_gameObject->GetScale();
	Vector2 delta;

	for (int i = m_properties.outlines.size() - 1; i >= 0; --i) {
		delta = m_properties.outlines[i].delta;
		m_font->DrawString(m_spriteBatchGet, m_properties.text, position + Vector2(delta.x, delta.y), m_properties.outlines[i].color, rotation, m_origin, scale);
		m_font->DrawString(m_spriteBatchGet, m_properties.text, position + Vector2(delta.x, -delta.y), m_properties.outlines[i].color, rotation, m_origin, scale);
		m_font->DrawString(m_spriteBatchGet, m_properties.text, position + Vector2(-delta.x, -delta.y), m_properties.outlines[i].color, rotation, m_origin, scale);
		m_font->DrawString(m_spriteBatchGet, m_properties.text, position + Vector2(-delta.x, delta.y), m_properties.outlines[i].color, rotation, m_origin, scale);
	}
}
