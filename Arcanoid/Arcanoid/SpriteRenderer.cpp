#include "pch.h"
#include "SpriteRenderer.h"
#include "Debugger.h"
#include "Game.h"
#include "GameObject.h"

using namespace DirectX;

SpriteRenderer::SpriteRenderer(const SpriteProperties& properties)
	:m_properties(properties) {}

void SpriteRenderer::LoadResources(const DeviceResourcesRef deviceResources)
{
	if(!m_deviceResources) 
		m_deviceResources = deviceResources;

	if (!m_resourceDescriptors) {
		auto device = deviceResources->GetD3DDevice();
		m_resourceDescriptors = std::make_unique<DescriptorHeap>(device, Descriptors::Count);
	}

	LoadTexture();
}

void SpriteRenderer::LoadTexture(bool firstTime)
{
	auto device = m_deviceResources->GetD3DDevice();
	ResourceUploadBatch resourceUpload(device);
	resourceUpload.Begin();

	try {
		DX::ThrowIfFailed(
			CreateWICTextureFromFile(device, resourceUpload, m_properties.texture,
				m_texture.ReleaseAndGetAddressOf()));
	}
	catch (...) {
		Debugger::Print(L"SpriteRenderer", L"Cannot find texture", m_properties.texture, LOG_SEVERITY::ERR);
		auto uploadResourcesFinished = resourceUpload.End(
			m_deviceResources->GetCommandQueue());
		uploadResourcesFinished.wait();
		bDeleted = true;
		return;
	}
	auto uploadResourcesFinished = resourceUpload.End(
		m_deviceResources->GetCommandQueue());

	uploadResourcesFinished.wait();

	CreateShaderResourceView(device, m_texture.Get(),
		m_resourceDescriptors->GetCpuHandle(Descriptors::Sprite));

	XMUINT2 spriteSize = GetTextureSize(m_texture.Get());

	m_size = Vector2(spriteSize.x, spriteSize.y);
	m_gameObject->SetSize(m_size);
	m_properties.origin.x = float(spriteSize.x / 2);
	m_properties.origin.y = float(spriteSize.y / 2);

	if (firstTime) {

		RenderTargetState rtState(m_deviceResources->GetBackBufferFormat(),
			m_deviceResources->GetDepthBufferFormat());

		SpriteBatchPipelineStateDescription pd(
			rtState,
			&CommonStates::NonPremultiplied
		);
		m_spriteBatch = std::make_unique<SpriteBatch>(device, resourceUpload, pd);

		auto viewport = m_deviceResources->GetScreenViewport();
		m_spriteBatch->SetViewport(viewport);

		Vector2 nextPosition = m_gameObject->GetPosition();
		auto size = m_deviceResources->GetOutputSize();
		nextPosition.x += float(size.right) / 2.f;
		nextPosition.y += float(size.bottom) / 2.f;
		m_gameObject->SetPosition(nextPosition);
	}
}

void SpriteRenderer::Reset()
{
	m_texture.Reset();
	m_spriteBatch.reset();
	m_resourceDescriptors.reset();
}

SpriteRenderer::Vector2 SpriteRenderer::GetSize() const
{
	return m_size;
}

void SpriteRenderer::SetSprite(const wchar_t* sprite)
{
	m_properties.texture = sprite;
	LoadTexture(false);
}

void SpriteRenderer::Render(const DeviceResourcesRef deviceResources)
{
	ID3D12DescriptorHeap* heaps[] = { m_resourceDescriptors->Heap() };

	auto commandList = deviceResources->GetCommandList();
	commandList->SetDescriptorHeaps(static_cast<UINT>(std::size(heaps)), heaps);

	m_spriteBatch->Begin(commandList);

	m_spriteBatch->Draw(
		m_resourceDescriptors->GetGpuHandle(Descriptors::Sprite),
		GetTextureSize(m_texture.Get()),
		m_gameObject->GetPosition(),
		nullptr,
		Colors::White,
		m_gameObject->GetRotation(),
		m_properties.origin,
		m_gameObject->GetScale()
	);

	m_spriteBatch->End();
}
