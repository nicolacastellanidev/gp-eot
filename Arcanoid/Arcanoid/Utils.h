#pragma once
class GameObject;
namespace Utils {

	typedef DirectX::SimpleMath::Vector2 Vector2;
	bool CheckCollision(const Vector2& firstPosition, const Vector2& secondPosition, const Vector2& firstSize, const Vector2& secondSize);
}
