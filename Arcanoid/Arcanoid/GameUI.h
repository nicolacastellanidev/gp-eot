#pragma once
#include "GameObject.h"
class HeartIcon;
class Player;

class GameUI :
    public GameObject
{

public:
    GameUI(const wchar_t* levelName);
    void Start() override;
    void AfterPlayerHit();

private:

    std::vector<std::shared_ptr<HeartIcon>> m_heartIcons;
    Player* m_player = nullptr;
};

