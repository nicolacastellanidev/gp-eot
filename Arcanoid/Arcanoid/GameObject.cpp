#include "pch.h"
#include "GameObject.h"
#include "Debugger.h"
#include "DeviceResources.h"
#include <typeinfo>
#include <SpriteRenderer.h>
#include "Engine.h"

using namespace DirectX;
using Microsoft::WRL::ComPtr;

GameObject::GameObject()
{
	m_transform = std::make_shared<Transform>();
}

GameObject::GameObject(const Vector2& position)
{
	m_transform = std::make_shared<Transform>(Transform(position));
}

GameObject::GameObject(const Vector2& position, float rotation)
{
	m_transform = std::make_shared<Transform>(Transform(position, rotation));
}

GameObject::GameObject(const Vector2& position, float rotation, const Vector2& scale)
{
	m_transform = std::make_shared<Transform>(Transform(position, rotation, scale));
}

GameObject::~GameObject()
{
	m_onDeathCb.clear();
	m_transform.reset();
	m_components.clear();
}

GameObject::TransformRef GameObject::GetTransform() const
{
	return m_transform;
}

GameObject::Components GameObject::GetComponents() const
{
	return m_components;
}

const GameObject::Vector2& GameObject::GetPosition() const
{
	return m_transform->GetPosition();
}

void GameObject::SetPosition(const Vector2& position) const
{
	m_transform->SetPosition(position);
}

float GameObject::GetRotation() const
{
	return m_transform->GetRotation();
}

void GameObject::SetRotation(float rotation) const
{
	m_transform->SetRotation(rotation);
}

const GameObject::Vector2& GameObject::GetScale() const
{
	return m_transform->GetScale();
}

void GameObject::SetScale(const Vector2& scale) const
{
	m_transform->SetScale(scale);
}

const GameObject::Vector2& GameObject::GetSize() const
{
	return m_size;
}

void GameObject::SetSize(const Vector2& size)
{
	m_size = size;
}

void GameObject::LoadResources(const DeviceResourcesRef deviceResources)
{
	for (ComponentRef component : m_components) {
		component->LoadResources(deviceResources);
	}

	for (std::shared_ptr<GameObject> m_children: m_childrens) {
		m_children->LoadResources(deviceResources);
	}
}

void GameObject::Reset()
{
	if (bDestroyed) return;
	for (ComponentRef component : m_components) {
		if(!component->IsDeleted()) component->Reset();
	}
	for (std::shared_ptr<GameObject> m_children : m_childrens) {
		m_children->Reset();
	}
}

void GameObject::Awake()
{
	for (ComponentRef component : m_components) {
		if (!component->IsDeleted()) component->Awake();
	}
	for (std::shared_ptr<GameObject> m_children : m_childrens) {
		m_children->Awake();
	}
}

void GameObject::Start()
{
	if (bDestroyed) return;
	for (ComponentRef component : m_components) {
		if (!component->IsDeleted()) component->Start();
	}
	for (std::shared_ptr<GameObject> m_children : m_childrens) {
		m_children->Start();
	}
}

void GameObject::Update()
{
	if (bStopped || bDestroyed) return;
	for (ComponentRef component : m_components) {
		if (!component->IsDeleted()) component->Update();
	}
	for (std::shared_ptr<GameObject> m_children : m_childrens) {
		m_children->Update();
	}
}

void GameObject::FixedUpdate()
{
	if (bStopped || bDestroyed) return;
	for (ComponentRef component : m_components) {
		if (!component->IsDeleted()) component->FixedUpdate();
	}
	for (std::shared_ptr<GameObject> m_children : m_childrens) {
		m_children->FixedUpdate();
	}
}

void GameObject::Render(const DeviceResourcesRef deviceResources)
{
	if (bDestroyed) { return; }
	for (ComponentRef component : m_components) {
		if (!component->IsDeleted()) component->Render(deviceResources);
	}
	for (std::shared_ptr<GameObject> m_children : m_childrens) {
		m_children->Render(deviceResources);
	}
}

void GameObject::Destroy()
{
	bDestroyed = true;
	Stop();
	for (unsigned int i = 0; i < m_onDeathCb.size(); ++i)
	{
		m_onDeathCb[i]();
	}
	for (ComponentRef component : m_components) {
		component->Destroy();
	}
	for (std::shared_ptr<GameObject> m_children : m_childrens) {
		m_children->Destroy();
	}
	Reset();
}

void GameObject::DestroyImmediate()
{
	m_onDeathCb.clear();
	Destroy();
}

void GameObject::OnDestroy(GameObject::DeathCallback cb)
{
	m_onDeathCb.push_back(cb);
}

void GameObject::Stop()
{
	bStopped = true;
	for (std::shared_ptr<GameObject> m_children : m_childrens) {
		m_children->Stop();
	}
}

void GameObject::AddComponent(ComponentRef component)
{
	m_components.push_back(component);
	component->Set(*this);
}

void GameObject::SetTag(TAG tag)
{
	m_tag = tag;
}

bool GameObject::CompareTag(TAG tag)
{
	return m_tag == tag;
}
