#include "pch.h"
#include "BrickController.h"
#include "BallController.h"
#include "SpriteRenderer.h"
#include "GameObject.h"
#include "Utils.h"
#include "ScreenManager.h"
#include "Debugger.h"
#include "Ball.h"
#include "Engine.h"

BrickController::BrickController(std::shared_ptr<SpriteRenderer> sr)
{
	m_sr = sr;
}

void BrickController::Start()
{
	m_ball = reinterpret_cast<Ball*>(Engine::GetGameObjectWithTag(TAG::BALL));

	if (!m_ball) {
		Debugger::Print(L"BrickController", L"Cannot find ball controller", LOG_SEVERITY::ERR);
		m_gameObject->Destroy();
	}

	m_size = m_sr->GetSize();
}

void BrickController::FixedUpdate()
{
	CheckBallPosition();
}

void BrickController::CheckBallPosition()
{
	auto ballPosition = m_ball->GetPosition();
	auto ballDirection = m_ball->GetDirection();
	auto mPosition = m_gameObject->GetPosition();

	if (Utils::CheckCollision(mPosition, ballPosition, m_size, m_ball->GetSize())) {
		m_ball->OnCollisionWithBrick(mPosition);
		m_gameObject->Destroy();
	}
}
