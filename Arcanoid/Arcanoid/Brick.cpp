#include "pch.h"
#include "Brick.h"
#include "SpriteRenderer.h"
#include "BrickController.h"
#include "Engine.h"
#include "DebugBoxRenderer.h"

Brick::Brick()
{
	m_id = L"Brick";
}

void Brick::SetType(BRICK_TYPE type)
{
	m_type = type;
}

void Brick::Awake()
{
	auto sr = std::make_shared<SpriteRenderer>(SpriteRenderer({ GetSprite() }));
	AddComponent(sr);
	AddComponent(std::make_shared<BrickController>(BrickController(sr)));

#ifdef DEBUG
	AddComponent(std::make_shared<DebugBoxRenderer>(DebugBoxRenderer()));
#endif // DEBUG

	SetTag(TAG::BRICK);
}

const wchar_t* Brick::GetSprite()
{
	switch (m_type)
	{
	default:
	case BRICK_TYPE::RED:
		return RED_BRICK;
	case BRICK_TYPE::YELLOW:
		return YELLOW_BRICK;
	}
}
