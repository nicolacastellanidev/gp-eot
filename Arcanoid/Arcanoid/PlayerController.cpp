#include "pch.h"
#include "PlayerController.h"
#include "InputManager.h"
#include "GameObject.h"
#include "Debugger.h"
#include "SpriteRenderer.h"
#include "ScreenManager.h"
#include "Ball.h"
#include "Utils.h"
#include "Engine.h"

using namespace DirectX::SimpleMath;

PlayerController::PlayerController(std::shared_ptr<SpriteRenderer> sr)
{
	m_sr = sr;
}

void PlayerController::Start()
{
	m_ball = reinterpret_cast<Ball*>(Engine::GetGameObjectWithTag(TAG::BALL));

	if (!m_ball) {
		Debugger::Print(L"PlayerController", L"Cannot find ball controller", LOG_SEVERITY::ERR);
		m_gameObject->Destroy();
	}

	m_size = m_sr->GetSize();
}

void PlayerController::Update()
{
	Move();
}

void PlayerController::FixedUpdate()
{
	CheckBallPosition();
}

void PlayerController::Move() {
	auto position = m_gameObject->GetPosition();
	float screenWidth = ScreenManager::GetWidth();

	m_speed.x = InputManager::GetAxisRaw(AXIS::HORIZONTAL) * BASE_SPEED;
	position.x += m_speed.x;

	if (position.x < 0 + m_size.x / 2) {
		position.x = 0 + m_size.x / 2;
	}

	if (position.x > screenWidth - m_size.x / 2) {
		position.x = screenWidth - m_size.x / 2;
	}

	m_gameObject->SetPosition(position);
}

void PlayerController::CheckBallPosition()
{
	if (!m_ball) {
		return;
	}

	auto ballPosition = m_ball->GetPosition();

	if (ballPosition.y < ScreenManager::GetHalfHeight()) {
		return; // ball is over the half of the screen, skip check
	}

	auto ballDirection = m_ball->GetDirection();
	auto mPosition = m_gameObject->GetPosition();
	auto dot = ballDirection.Dot(TRANSFORM_UP);

	if (dot < 0) { // opposite direction

		// collision only if on both axes
		if (Utils::CheckCollision(mPosition, ballPosition, m_size, m_ball->GetSize())) {
			m_ball->OnCollisionWithPlayer(mPosition, m_speed.x);
		}

	}
}
