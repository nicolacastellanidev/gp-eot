#include "pch.h"
#include "HeartIcon.h"
#include "SpriteRenderer.h"

HeartIcon::HeartIcon()
{
	m_id = L"HeartIcon";
}

void HeartIcon::Awake()
{
	// m_sr = std::make_shared<SpriteRenderer>(SpriteRenderer({ L"./Assets/Sprites/hearts.dds" }));
	m_sr = std::make_shared<SpriteRenderer>(SpriteRenderer({ L"./Assets/Sprites/hearts.png" }));
	AddComponent(m_sr);
	GameObject::Awake();
}

void HeartIcon::SetSprite(const wchar_t* sprite)
{
	m_sr->SetSprite(sprite);
}
