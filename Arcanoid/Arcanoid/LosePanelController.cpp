#include "pch.h"
#include "LosePanelController.h"
#include "SceneManager.h"
#include "InputManager.h"
#include "GameObject.h"

void LosePanelController::Update()
{
	Component::Update();

	auto kb = InputManager::GetKeyboardState();

	if (kb.Escape) return;	// esc is handled by Game.cpp
	if (kb.Space) {
		SceneManager::ReloadCurrentScene();
		m_gameObject->Destroy();
	}
}
