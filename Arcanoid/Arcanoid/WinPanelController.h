#pragma once
#include "Component.h"

class SpriteRenderer;

class WinPanelController :
    public Component
{
public:
    void Update() override;
};

