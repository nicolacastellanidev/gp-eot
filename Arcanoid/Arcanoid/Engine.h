#pragma once
class GameObject;

enum class TAG {
	NONE,
	BALL,
	PLAYER,
	BRICK
};

class Engine
{
	static Engine* instance;

	typedef std::vector<GameObject*> GameObjects;
public:
	static Engine* GetInstance();
	/**
	* Register GameObject
	* @author nicola.castellani
	*/
	static void AddGameObject(GameObject* go);
	/**
	* @return game object with a specific tag, nullptr if found nothing
	* @author nicola.castellani
	*/
	static GameObject* GetGameObjectWithTag(TAG tag);
	/**
	* Clears registered game objects (NOTE: doesn't affect GO lifecycle)
	* @author nicola.castellani
	*/
	static void ResetGameObjects();
private:
	GameObjects m_gameObjects;
};

