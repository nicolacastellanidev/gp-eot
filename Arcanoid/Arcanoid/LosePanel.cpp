#include "pch.h"
#include "LosePanel.h"
#include "SpriteRenderer.h"
#include "LosePanelController.h"

LosePanel::LosePanel()
{
	m_id = L"LosePanel";
}

void LosePanel::Awake()
{
	// AddComponent(std::make_shared<SpriteRenderer>(SpriteRenderer({ L"./Assets/Sprites/losepanel.dds" })));
	AddComponent(std::make_shared<SpriteRenderer>(SpriteRenderer({ L"./Assets/Sprites/losepanel.png" })));
	AddComponent(std::make_shared<LosePanelController>(LosePanelController()));

	GameObject::Awake();
}
