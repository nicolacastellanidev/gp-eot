#pragma once
#include "BaseLevel.h"

class Level02 :
	public BaseLevel
{
private:
	void Load(const DeviceResources deviceResources) override;
	void CreateBricks() override;
};