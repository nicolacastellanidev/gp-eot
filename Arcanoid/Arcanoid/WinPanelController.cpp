#include "pch.h"
#include "WinPanelController.h"
#include "InputManager.h"
#include "Debugger.h"
#include "SceneManager.h"
#include "GameObject.h"

void WinPanelController::Update()
{
	Component::Update();

	auto kb = InputManager::GetKeyboardState();
	
	if (kb.Escape) return;	// esc is handled by Game.cpp
	if (kb.Space) {
		SceneManager::LoadNextScene();
		m_gameObject->Destroy();
	}
}