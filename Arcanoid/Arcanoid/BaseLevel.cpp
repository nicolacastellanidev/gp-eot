#include "pch.h"
#include "BaseLevel.h"
#include "GameObject.h"
#include "WinPanelController.h"
#include "Background.h"
#include "Ball.h"
#include "Player.h"
#include "Brick.h"
#include "GameUI.h"
#include "WinPanel.h"
#include "SceneManager.h"
#include "LosePanel.h"

using namespace DirectX;

void BaseLevel::Load(const DeviceResources deviceResources)
{
	m_deviceResources.reset();
	m_deviceResources = deviceResources;
	LoadScene();
	Scene::Load(deviceResources);
	SceneManager::SceneLoaded();
}

void BaseLevel::Unload()
{
	// restore original values
	brickCount = 0;
	playerHealth = 3;
	Scene::Unload();
}

void BaseLevel::LoadScene() {
	// Background
	auto bg = std::make_shared<Background>(Background());
	m_gameObjects.push_back(bg); // placed at origin

	// ball
	auto ball = std::make_shared<Ball>(Ball());
	ball->SetPosition({ 0, 200 });
	m_gameObjects.push_back(ball); // placed at origin

	// player
	auto player = std::make_shared<Player>(Player());
	player->SetPosition({ 0, 350 });
	player->OnDestroy(std::bind(&BaseLevel::AfterPlayerDestroy, this));
	m_gameObjects.push_back(player);

	// bricks
	CreateBricks();

	// game ui
	auto ui = std::make_shared<GameUI>(GameUI(m_id));
	m_gameObjects.push_back(ui); // placed at origin
}

void BaseLevel::CreateBricks()
{
	// implement in custom levels
}

void BaseLevel::CreateBrick(const Vector2& position)
{
	auto brick = std::make_shared<Brick>(Brick());

	brick->SetPosition(position);

	if ((int)position.y % 2 == 0.0f) {
		brick->SetType(BRICK_TYPE::YELLOW);
	}

	brick->OnDestroy(std::bind(&BaseLevel::AfterBrickDestroy, this));

	m_gameObjects.push_back(brick);
	brickCount++;
}

void BaseLevel::AfterBrickDestroy()
{
	if (brickCount <= 0) {
		return;
	}

	brickCount--;

	if (brickCount == 0) {
		for each (std::shared_ptr<GameObject> go  in m_gameObjects)
		{
			go->Stop();
		}

		// Win panel
		auto winPanel = std::make_shared<WinPanel>(WinPanel());
		winPanel->Awake();
		winPanel->LoadResources(m_deviceResources);
		m_gameObjects.push_back(winPanel);
	}
}

void BaseLevel::AfterPlayerDestroy()
{
	for each (std::shared_ptr<GameObject> go  in m_gameObjects)
	{
		go->Stop();
	}
	// Win panel
	auto losePanel = std::make_shared<LosePanel>(LosePanel());
	losePanel->Awake();
	losePanel->LoadResources(m_deviceResources);
	m_gameObjects.push_back(losePanel);
}
