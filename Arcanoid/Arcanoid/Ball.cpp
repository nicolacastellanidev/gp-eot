#include "pch.h"
#include "Ball.h"
#include "SpriteRenderer.h"
#include "BallController.h"
#include "Engine.h"
#include "DebugBoxRenderer.h"

Ball::Ball()
{
	m_id = L"Ball";
}

void Ball::Awake()
{
	// 	m_sr = std::make_shared<SpriteRenderer>(SpriteRenderer({ L"./Assets/Sprites/ball.dds" }));
	m_sr = std::make_shared<SpriteRenderer>(SpriteRenderer({ L"./Assets/Sprites/ball.png" }));
	m_bc = std::make_shared<BallController>(BallController(m_sr));
	AddComponent(m_sr);
	AddComponent(m_bc);

#ifdef DEBUG
	AddComponent(std::make_shared<DebugBoxRenderer>(DebugBoxRenderer()));
#endif // DEBUG

	SetTag(TAG::BALL);
	Engine::AddGameObject(this); // register
}

const Ball::Vector2& Ball::GetDirection()
{
	return m_bc->GetDirection();
}

const Ball::Vector2& Ball::GetSize()
{
	return m_bc->GetSize();
}

void Ball::OnCollisionWithPlayer(const Vector2& playerPosition, float playerXSpeed)
{
	m_bc->OnCollisionWithPlayer(playerPosition, playerXSpeed);
}

void Ball::OnCollisionWithBrick(const Vector2& brickPosition)
{
	m_bc->OnCollisionWithBrick(brickPosition);
}
