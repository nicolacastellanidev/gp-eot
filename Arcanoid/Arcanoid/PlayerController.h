#pragma once
#include "Component.h"

class SpriteRenderer;
class Ball;

class PlayerController :
    public Component
{
    typedef DirectX::SimpleMath::Vector2 Vector2;

#ifndef BASE_SPEED
#define BASE_SPEED 15.0f
#endif

public:
    PlayerController(std::shared_ptr<SpriteRenderer> sr);

protected:
    void Start() override;
    void Update() override;
    void FixedUpdate() override;
    void Move();
    void CheckBallPosition();
    std::shared_ptr<SpriteRenderer> m_sr;

private:
    Vector2 m_size;
    Vector2 m_speed{ 0,0 };
    Ball* m_ball;
};

