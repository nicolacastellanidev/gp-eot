#pragma once
#include <SimpleMath.h>

#ifndef TRANSFORM_UP
#define TRANSFORM_UP {0, 1}
#endif

class Transform
{
	typedef DirectX::SimpleMath::Vector2 Vector2;

public:
	Transform() : m_position(Vector2{ 0,0 }), m_rotation(0), m_scale(Vector2{ 1, 1 }) {}
	Transform(const Vector2& position) : m_position(position), m_rotation(0), m_scale(Vector2{ 1, 1 }) {}
	Transform(const Vector2& position, float rotation) : m_position(position), m_rotation(rotation), m_scale(Vector2{ 1, 1 }) {}
	Transform(const Vector2& position, float rotation, const Vector2& scale) : m_position(position), m_rotation(rotation), m_scale(scale) {}

	// getters and setters
	const Vector2& GetPosition() const;
	void SetPosition(const Vector2& position);

	float GetRotation() const;
	void SetRotation(float rotation);

	const Vector2& GetScale() const;
	void SetScale(const Vector2& scale);

private:
	Vector2 m_position;
	Vector2 m_scale;
	float m_rotation;
};

