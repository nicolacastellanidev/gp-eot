#include "pch.h"
#include "Utils.h"
#include "GameObject.h"

bool Utils::CheckCollision(const Vector2& firstPosition, const Vector2& secondPosition, const Vector2& firstSize, const Vector2& secondSize)
{
	return (abs(firstPosition.x - secondPosition.x) * 2 < (firstSize.x + secondSize.x)) &&
		(abs(firstPosition.y - secondPosition.y) * 2 < (firstSize.y + secondSize.y));
}
