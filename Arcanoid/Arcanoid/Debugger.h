#pragma once
#include "TextConsole.h"
#include "DeviceResources.h"

#ifndef CONSOLE_ROW_LIMIT
#define CONSOLE_ROW_LIMIT 15
#endif // !CONSOLE_ROW_LIMIT

enum class LOG_SEVERITY : std::uint8_t {
	LOG,
	WARN,
	ERR
};
const wchar_t* ToString(const LOG_SEVERITY Log);

class Debugger {
	enum Descriptors
	{
		Font,
		Count
	};
	static Debugger* instance;

	typedef DirectX::SimpleMath::Vector2 Vector2;
	typedef std::unique_ptr<DX::TextConsole> TextConsoleRef;
	typedef std::unique_ptr<DirectX::DescriptorHeap> DescriptorHeapRef;
	typedef std::shared_ptr<DX::DeviceResources> DeviceResourcesRef;

public:
	static Debugger* GetInstance();

	static void Init(const DeviceResourcesRef deviceResources);
	static void Render(const DeviceResourcesRef deviceResources);
	static void SetConsoleViewport(const DeviceResourcesRef deviceResources);
	static void Reset();

	static void Print(const wchar_t* Context, const wchar_t* Message, const LOG_SEVERITY Log);
	static void Print(const wchar_t* Context, const wchar_t* Message, const wchar_t* Description, const LOG_SEVERITY Log);
	static void Print(const wchar_t* Context, const wchar_t* Message, const Vector2& Vector, const LOG_SEVERITY Log);
	static void Print(const wchar_t* Context, const wchar_t* Message, float value, const LOG_SEVERITY Log);

	static void Clear();

private:
	static DirectX::XMVECTORF32 GetConsoleAttributes(const LOG_SEVERITY Log);

	TextConsoleRef console;
	DescriptorHeapRef m_resourceDescriptors;

	unsigned int m_rows = 0;
};
