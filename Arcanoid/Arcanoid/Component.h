#pragma once
#include <memory>

// FW
class GameObject;
namespace DX {
	class DeviceResources;
}

class Component
{
	typedef std::shared_ptr<DX::DeviceResources> DeviceResourcesRef;
	typedef GameObject* GameObjectRef;

protected:
	friend class GameObject;
	/**
	* Called when scene is loaded
	* @author nicola.castellani
	*/
	virtual void Awake() {};
	/**
	* Called when game starts
	* @author nicola.castellani
	*/
	virtual void Start() {};
	/**
	* Called every game loop tick
	* @author nicola.castellani
	*/
	virtual void Update() {};
	/**
	* Called every window tick
	* @author nicola.castellani
	*/
	virtual void FixedUpdate() {};
	/**
	* Removes the GameObject (stop rendering)
	* @author nicola.castellani
	*/
	virtual void Destroy();
	/**
	* getter for bDeleted
	* @author nicola.castellani
	*/
	virtual bool IsDeleted() { return bDeleted; }
	/**
	* @eturn component GameObject instance
	* @author nicola.castellani
	*/
	virtual const GameObject& GetGameObject();
	/**
	* Sets parent game object
	* @author nicola.castellani
	*/
	virtual void Set(GameObject& gameObject);
	/**
	* Load the needed resources (e.g. textures, fonts, etc..)
	* @author nicola.castellani
	*/
	virtual void LoadResources(const DeviceResourcesRef deviceResources) {};
	/**
	* Reset pointers
	* @author nicola.castellani
	*/
	virtual void Reset() {};
	/**
	* Called after Update
	* @author nicola.castellani
	*/
	virtual void Render(const DeviceResourcesRef deviceResources) {};

	GameObjectRef m_gameObject;
	bool bDeleted = false;
};