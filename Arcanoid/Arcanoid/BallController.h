#pragma once
#include "Component.h"

class SpriteRenderer;
class Player;

class BallController :
    public Component
{
	typedef DirectX::SimpleMath::Vector2 Vector2;
#ifndef BASE_SPEED
#define BASE_SPEED 7.0f
#endif
public:
	BallController(std::shared_ptr<SpriteRenderer> sr);
	
	void OnCollisionWithPlayer(const Vector2& playerPosition, float playerXSpeed);
	void OnCollisionWithBrick(const Vector2& brickPosition);
	void SetHorizontalDirection(int next);

	float Radius();
	const Vector2& GetSize();
	const Vector2& GetDirection();
protected:
	void Start() override;
	void Update() override;
	std::shared_ptr<SpriteRenderer> m_sr;

private:
	Vector2 m_speed{ BASE_SPEED, -BASE_SPEED };
	Vector2 m_size;
	Vector2 m_direction;
	Player* m_player;
	float m_radius;
};

