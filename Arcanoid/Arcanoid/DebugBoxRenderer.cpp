#include "pch.h"
#include "DebugBoxRenderer.h"
#include "DeviceResources.h"
#include "ScreenManager.h"
#include "GameObject.h"
#include <PrimitiveBatch.h>

using namespace DirectX;
using namespace DirectX::SimpleMath;
using namespace DirectX::Colors;

#ifndef CROSS_LENGTH
#define CROSS_LENGTH 5.0f
#endif

void DebugBoxRenderer::LoadResources(const std::shared_ptr<DX::DeviceResources> deviceResources)
{
	auto device = deviceResources->GetD3DDevice();

	m_resourceDescriptors = std::make_unique<DescriptorHeap>(device, Descriptors::Count);

	std::unique_ptr<PrimitiveBatch<VertexPositionColor>> primitiveBatch;
	m_debugBatch = std::make_unique<PrimitiveBatch<VertexPositionColor>>(device);

	std::unique_ptr<BasicEffect> lineEffect;

	RenderTargetState rtState(deviceResources->GetBackBufferFormat(),
		deviceResources->GetDepthBufferFormat());

	EffectPipelineStateDescription pd(
		&VertexPositionColor::InputLayout,
		CommonStates::Opaque,
		CommonStates::DepthDefault,
		CommonStates::CullNone,
		rtState,
		D3D12_PRIMITIVE_TOPOLOGY_TYPE_LINE
	);

	m_effect = std::make_unique<BasicEffect>(device,
		EffectFlags::VertexColor,
		pd);

	m_effect->SetProjection(XMMatrixOrthographicOffCenterRH(0,
		ScreenManager::GetWidth(), ScreenManager::GetHeight(), 0, 0, 1));
}

void DebugBoxRenderer::Reset()
{
	m_debugBatch.reset();
}

void DebugBoxRenderer::Render(const std::shared_ptr<DX::DeviceResources> deviceResources)
{
	ID3D12DescriptorHeap* heaps[] = { m_resourceDescriptors->Heap() };

	auto commandList = deviceResources->GetCommandList();
	commandList->SetDescriptorHeaps(static_cast<UINT>(std::size(heaps)), heaps);

	auto m_position = m_gameObject->GetPosition();
	auto m_size = m_gameObject->GetSize();

	float x = m_position.x;
	float y = m_position.y;
	float sizeX = m_size.x / 2.0f;
	float sizeY = m_size.y / 2.0f;

	m_effect->Apply(commandList);
	m_debugBatch->Begin(commandList);

	// top line
	m_debugBatch->DrawLine({ Vector3( x - sizeX, y - sizeY, 0.f), GreenYellow },{ Vector3( x + sizeX, y - sizeY, 0.f), GreenYellow });
	// bottom line
	m_debugBatch->DrawLine({ Vector3( x - sizeX, y + sizeY, 0.f), GreenYellow },{ Vector3( x + sizeX, y + sizeY, 0.f), GreenYellow });
	// left line
	m_debugBatch->DrawLine({ Vector3( x - sizeX, y - sizeY, 0.f), GreenYellow },{ Vector3( x - sizeX, y + sizeY, 0.f), GreenYellow });
	// right line
	m_debugBatch->DrawLine({ Vector3( x + sizeX, y - sizeY, 0.f), GreenYellow },{ Vector3( x + sizeX, y + sizeY, 0.f), GreenYellow });
	// center
	m_debugBatch->DrawLine({ Vector3( x - CROSS_LENGTH, y - CROSS_LENGTH, 0.f), Red }, { Vector3( x + CROSS_LENGTH, y + CROSS_LENGTH, 0.f), Red });
	m_debugBatch->DrawLine({ Vector3( x + CROSS_LENGTH, y - CROSS_LENGTH, 0.f), Red }, { Vector3( x - CROSS_LENGTH, y + CROSS_LENGTH, 0.f), Red });

	m_debugBatch->End();
}
