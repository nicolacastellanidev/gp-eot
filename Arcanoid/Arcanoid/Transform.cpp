#include "pch.h"
#include "Transform.h"

const Transform::Vector2& Transform::GetPosition() const
{
	return m_position;
}

void Transform::SetPosition(const Vector2& position)
{
	m_position = position;
}

float Transform::GetRotation() const
{
	return m_rotation;
}

void Transform::SetRotation(float rotation)
{
	m_rotation = rotation;
}

const Transform::Vector2& Transform::GetScale() const
{
	return m_scale;
}

void Transform::SetScale(const Vector2& scale)
{
	m_scale = scale;
}
